const express = require('express');
let router = express.Router();
const Messages = require('../models/message');

router.get('/', (req, res, next) => {
  Messages.find({}, (err, messages) => {
    if (err) {
      return next(err);
    }
    res.json(messages);
  })
});

router.post('/', (req, res, next) => {
  const data = req.body;
  console.log("POST DATA: ", data);
  if (Object.keys(data) == 0) return;
  let message = new Messages(
    {data: data}
  );

  message.save((err, message) => {
    console.log("ITEM HAS BEEN ADDED:  ", message);
    if(err) {
      res.json({'ERROR': err});
    } else {
      res.json(message);
    }
  });
});

router.put('/:id', (req, res, next) => {
  Messages.findByIdAndUpdate( req.params.id, {data : req.body}, {new: true}, (err, message) => {
    if(err) {
      res.json({'ERROR': err});
    } else {
      res.json(message);
    }
  });
})

router.delete('/:id', (req, res, next) => {
  console.log("req.params.id: ", req.params.id);
  Messages.findOneAndDelete({'_id':req.params.id}, (err, message) => {
    if (err) return next(err);
    console.log("ITEM HAS BEEN DELETED:  ", message);
    if(err) {
      res.json({'ERROR': err});
    } else {
      res.json(message);
    }
  })
});

module.exports = router;