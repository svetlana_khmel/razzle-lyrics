import React from 'react';
import Route from 'react-router-dom/Route';
import Switch from 'react-router-dom/Switch';
import Home from './Home';
import Login from './Login';
import NewPost from './NewPost';
import EditPost from './EditPost';


import './App.css';

const App = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route exact path="/login" component={Login} />
    <Route exact path="/new" component={NewPost}/>
    <Route exact path="/edit" component={EditPost}/>
  </Switch>
);

export default App;
