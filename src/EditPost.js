import React, { Component } from 'react';
import { connect } from 'react-redux';
import { manageMarkedChipsCreator } from './actions';
import ChipsSelect from './components/chips-select';
import { editPost } from './actions/index';

class EditPost extends Component {
  constructor(props) {
    super(props);

    const {
      title,
      article,
      translation,
      category,
    } = this.props.message[0].data;

    this.state = {
      title,
      article,
      translation,
    };
  }

  componentDidMount() {
    const category = this.props.message[0].data.category.split(',');

    category.map(item => {
      this.markCategory(item, true);
    });
  }

  changeValue = (name, value) => {
    this.setState({
      [name]: value,
    });
  };

  // renderArticle = () => {
  //   const { article } = this.props.message[0].data;
  //   if (article) {
  //     const rawMarkup =
  //       typeof article === 'string' ? article : article.join('\n');
  //     return rawMarkup;
  //   }
  // };

  markCategory = (query, mark) => {
    this.props.manageMarkedChips(query, mark);
  };

  // renderTranslation = () => {
  //   const { translation } =  this.props.message[0].data;
  //   if (translation) {
  //     const rawMarkup =
  //       typeof translation === 'string'
  //         ? translation
  //         : translation.join('<br>');
  //     return rawMarkup;
  //   }
  // };

  submitData = () => {
    // const title = this.refs.title.value;
    // const lyrics = this.refs.lyrics.value;
    //const translation = this.refs.translation.value;
    const { _id } = this.props.message[0];
    const { markedChips } = this.props;

    const { title, article, translation } = this.state;

    const data = {
      title: /\n/.test(title) ? title.split(/\n/) : title || '',
      article: /\n/.test(article) ? article.split(/\n/) : article || '',
      translation: /\n/.test(translation)
        ? translation.split(/\n/)
        : translation || 'Some translation may be here...',
      category: markedChips.length === 0 ? '' : markedChips.join(),
    };

    this.props.saveEdit(data, _id);
    this.props.history.push('/');
  };

  render() {
    console.log('POST RENDER ', this.props);
    const { _id } = this.props.message[0];

    return (
      <div className={'new-post-block'}>
        <h2>New Post</h2>
        <div className={'new-post-form'}>
          <fieldset>
            <label>_id</label>
            <input
              className={'form-control'}
              type={'text'}
              ref={'id'}
              name="title"
              value={_id}
            />
          </fieldset>
          <fieldset>
            <label>Title</label>
            <input
              className={'form-control'}
              type={'text'}
              ref={'title'}
              name="title"
              value={this.state.title}
              onChange={e => this.changeValue('title', e.target.value)}
            />
          </fieldset>

          <fieldset>
            <label>Lyrics</label>
            <textarea
              className={'form-control'}
              ref={'lyrics'}
              name="lyrics"
              value={
                typeof this.state.article === 'string'
                  ? this.state.article
                  : this.state.article.join('\n')
              }
              onChange={e => this.changeValue('article', e.target.value)}
            />
          </fieldset>
          <fieldset>
            <label>Translation</label>
            <textarea
              className={'form-control'}
              ref={'translation'}
              name="translation"
              value={
                typeof this.state.translation === 'string'
                  ? this.state.translation
                  : this.state.translation.join('\n')
              }
              onChange={e => this.changeValue('translation', e.target.value)}
            />
          </fieldset>
          <fieldset>
            <label>Category</label>
            <ChipsSelect />
            <div className={'chips '} />
          </fieldset>

          <button id="something-btn" type="button" onClick={this.submitData}>
            Send
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { messages, choosenPost } = state.data;

  return {
    markedChips: state.data.markedChips || [],
    message: messages.filter(message => message._id === choosenPost),
  };
};

const mapDispatchToProps = dispatch => ({
  manageMarkedChips: (payload, mark) =>
    dispatch(manageMarkedChipsCreator(payload, mark)),
  saveEdit: (data, id) => dispatch(editPost(data, id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditPost);
