import React, { Component } from 'react';
import './css/Home.css';
import { Link } from 'react-router-dom';
import SimpleTabs from './components/material-ui/simple-tab';
import DrawerAppBar from './components/switch-drawer';
import LyricsBox from './containers/lyrics-box';

class Home extends Component {
  render() {
    return (
      <div className="Home">
        <div className="Home-header" style={{display: 'none'}}>
          <DrawerAppBar />
          <SimpleTabs parentProps={this.props} />
        </div>
        <LyricsBox />

        <Link to="/">home</Link>
        <Link to="/login">Login Page</Link>
      </div>
    );
  }
}

export default Home;
