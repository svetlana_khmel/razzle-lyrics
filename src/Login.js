import React, { Component } from 'react';
import { connect } from 'react-redux';

import LoginForm from './components/login-form';
import RegisterForm from './components/register-form';

import { loginUser, registerUser } from './actions/';

class Login extends Component {
  constructor () {
    super();
    this.handleSubmitLogin = this.handleSubmitLogin.bind(this);
    this.handleSubmitRegister = this.handleSubmitRegister.bind(this);

  }
  handleSubmitLogin () {
    this.props.login()
  }

  handleSubmitRegister () {
    this.props.register()
  }

  render(){
    return (
      <div>
        <LoginForm onSubmit={this.handleSubmitLogin} />
        <RegisterForm onSubmit={this.handleSubmitRegister} />
      </div>
      )
  }
}

const mapDispatchToProps = (dispatch) => ({
  login: () => dispatch(loginUser()),
  register: () => dispatch(registerUser()),
});

export default connect(null, mapDispatchToProps)(Login);