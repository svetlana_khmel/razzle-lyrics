import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createPost } from './actions/';
import ChipsSelect from './components/chips-select';

class NewPost extends Component {
  submitData = () => {
    const title = this.refs.title.value;
    const lyrics = this.refs.lyrics.value;
    const translation = this.refs.translation.value;
    const category = this.props.markedChips;

    const data = {
      title: /\n/.test(title) ? title.split(/\n/) : title || '',
      article: /\n/.test(lyrics) ? lyrics.split(/\n/) : lyrics || '',
      translation: /\n/.test(translation)
        ? translation.split(/\n/)
        : translation || 'Some translation may be here...',
        category: category.length === 0 ? '' : category.join()
    };

    this.props.create(data);
    this.props.history.push("/")
  };

  render() {
    console.log('EDIT POST: ', this.props);
    return (
      <div className={'new-post-block'}>
        <h2>New Post</h2>
        <div className={'new-post-form'}>
          <fieldset>
            <label>Title</label>
            <input
              className={'form-control'}
              type={'text'}
              ref={'title'}
              name="title"
            />
          </fieldset>
          <fieldset>
            <label>Lyrics</label>
            <textarea className={'form-control'} ref={'lyrics'} name="lyrics" />
          </fieldset>
          <fieldset>
            <label>Translation</label>
            <textarea
              className={'form-control'}
              ref={'translation'}
              name="translation"
            />
          </fieldset>
          <fieldset>
            <label>Category</label>
            <ChipsSelect />
            <div className={'chips '} />
          </fieldset>

          <button id="something-btn" type="button" onClick={this.submitData}>
            Send
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  markedChips: state.data.markedChips,
});

const mapDispatchToProps = dispatch => ({
  create: (payload) => dispatch(createPost(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewPost);
