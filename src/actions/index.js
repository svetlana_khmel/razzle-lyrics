export const SET_COUNTER = 'SET_COUNTER';
export const INCREMENT_COUNTER = 'INCREMENT_COUNTER';
export const DECREMENT_COUNTER = 'DECREMENT_COUNTER';
export const DO_SEADRCH = 'DO_SEADRCH';
export const ADD_MARKED_CHIPS = 'ADD_MARKED_CHIPS';
export const MANAGE_MARKED_CHIPS = 'MANAGE_MARKED_CHIPS';
export const UPDATE_DATA = 'UPDATE_DATA';
export const SET_ACTIVE_POST = 'SET_ACTIVE_POST';

export const doSearchCreator = (filtered, all) => {
  return {
    type: DO_SEADRCH,
    filtered,
    all
  }
};

export const manageMarkedChipsCreator = (payload, mark) => ({
  type: MANAGE_MARKED_CHIPS,
  payload,
  mark
});

export const updateData = (payload) =>{
  console.log('... updateData.action...');
  return {
  type: UPDATE_DATA,
  payload
}};

// export const updateData = (payload) =>({
//   type: UPDATE_DATA,
//   payload
// });
//


export const setChoosenPostCreator = (payload) => {
  console.log(payload);
  return {
  type: SET_ACTIVE_POST,
  payload
}};

// export const setChoosenPost = (payload) => ({
//   type: SET_ACTIVE_POST,
//   payload
// });
//


export const set = value => ({
  type: SET_COUNTER,
  payload: value,
});



// export const incrementIfOdd = () => (dispatch, getState) => {
//   const { counter } = getState();
//
//   if (counter % 2 === 0) {
//     return;
//   }
//
//   dispatch(increment());
// };
//
// export const incrementAsync = (delay = 1000) => dispatch => {
//   setTimeout(() => {
//     dispatch(increment());
//   }, delay);
// };

export const loginUser = () => (dispatch, getState) => {
  const { form } = getState();
  const { username, password } = form.loginForm.values;
  fetch('/api/login', {
    method: 'POST',
    body: JSON.stringify({username, password}),
    headers: { 'Content-Type': 'application/json' }
  })
    //.then(res => res.json()) // expecting a json response
    .then(json => console.log(json));
}

export const registerUser = () => (dispatch, getState) => {
  const { form } = getState();
  const { username, password } = form.registerForm.values;
  fetch('/api/register', {
    method: 'POST',
    body: JSON.stringify({username, password}),
    headers: { 'Content-Type': 'application/json' }
  })
    //.then(res => res.json()) // expecting a json response
    .then(json => console.log(json));
}

export const fetchData = () => (dispatch) => {
  console.log('fetchData .....');
  fetch('/api/messages', {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' }
  })
    .then(res => res.json())
    .then(json => {
      console.log(json);
      dispatch(updateData(json));
    })
}

export const createPost = (data) => (dispatch) => {
    fetch('/api/messages', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: { 'Content-Type': 'application/json' }
    })
   .then(res => res.json()) // expecting a json response
   .then(json => {
     console.log('createPost .....',json);
     dispatch(fetchData());
   });
}

export const editPost = (data, id) => (dispatch) => {
    console.log("!!!!!>>>>>>>>::::::: ", data);
    fetch(`/api/messages/${ id }`, {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: { 'Content-Type': 'application/json' }
    })
   .then(res => res.json()) // expecting a json response
   .then(json => {
     console.log('editPost .....',json);
     dispatch(fetchData());
   });
}


