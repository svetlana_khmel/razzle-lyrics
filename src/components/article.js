import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Chip from '@material-ui/core/Chip';
import uuidv1 from 'uuid/v1';

import { withRouter } from 'react-router-dom';

class Article extends Component {
  renderArticle = () => {
    const { article } = this.props.data;
    if (article) {
      const rawMarkup =
        typeof article === 'string' ? article : article.join('<br>');
      return { __html: rawMarkup };
    }
  };

  renderTranslation = () => {
    const { translation } = this.props.data;
    if (translation) {
      const rawMarkup =
        typeof translation === 'string'
          ? translation
          : translation.join('<br>');
      return { __html: rawMarkup };
    }
  };

  renderCategory = () => {
    const { category } = this.props.data;

    if (category) {
      return category.split(',').map(item => {
        return <Chip key={uuidv1()} label={item.toString()} variant="outlined" />
      });
    }
  };

  handleSubmit = () => {
    console.log('THIS PROPS ', this.id);
    this.props.setChoosenPost(this.id)
    this.props.history.push(`/edit`);
  };

  render() {
    const { title } = this.props.data;
    this.id = this.props.id;

   // console.log('THIS PROPS ', this.id);
    return (
      <Card className="comment">
        <CardContent>
          <h2>
            {title}
          </h2>
          <button onClick={this.handleSubmit}>Edit</button>
          <div>
            <span dangerouslySetInnerHTML={this.renderArticle()} />
            <div dangerouslySetInnerHTML={this.renderTranslation()} />
            <div>{ this.renderCategory()} </div>
          </div>
        </CardContent>
      </Card>
    );
  }
}

export default withRouter(Article);

//
// Article.propTypes = {
//  // data: PropTypes.arrayOf(PropTypes.any),
// };