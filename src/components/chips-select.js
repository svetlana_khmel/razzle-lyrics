import React, { Component } from 'react';
import { connect } from 'react-redux';
import { doSearchCreator, manageMarkedChipsCreator } from '../actions';
import PropTypes from 'prop-types';
import '../css/chips.css';

class ChipsSelect extends Component {
  state = {
    categories: []
  }

  searchCategory = e => {
    const query = e.target.dataset.category;
    let queryResult = [];
    const { messages } = this.props;

    messages.map(message => {
      if (
        message &&
        message.data &&
        message.data.category.toLowerCase().indexOf(query.toLowerCase()) !== -1
      )
        queryResult.push(message);
    });

    const data = {
      query,
      queryResult,
    };

    this.props.doSearch(data, messages);
  };

  markCategory = (e, mark) => {
    const query = e.target.dataset.category;

    this.props.manageMarkedChips(query, mark);
  };

  fillCategories = () => {
    let categoryValues = this.refs.category.value;

    this.setState({
      categories: categoryValues.trim(),
    });
  };

  addNewCategory = () => {
    this.props.manageMarkedChips(this.state.categories, true);
  }

  render() {
    let categories = [];

    const { messages, markedChips } = this.props;

    messages.map(el => {
      if (el.data) {
        if (!categories.includes(el.data.category)) {
          if (/,/.test(el.data.category)) {
            const arr = el.data.category.split(',');

            arr.map(element => {
              if (!categories.includes(element.trim())) {
                categories = [...categories, element.trim()];
              }
            });
          } else {
            categories = [...categories, el.data.category.trim()];
          }
        }
      }
    });

    return (
      <div>
        <div className="filter-block">
          {categories.length != 0 &&
            categories.map((el, index) => {
              if (el.length !== 0) {
                return (
                  <div
                    key={el + index}
                    data-category={el}
                    className="category-item filter"
                    onClick={(e) => this.markCategory(e, true)}
                  >
                    {el}
                  </div>
                );
              }
            })}
        </div>
        <div className="filter-block">
          {markedChips.length !== 0 &&
            markedChips.map((el, index) => {
              if (el.length !== 0) {
                return (
                  <div
                    key={el + index}
                    data-category={el}
                    className="category-item filter"
                    onClick={e => this.markCategory(e, false)}
                  >
                    {el}
                  </div>
                );
              }
            })}
        </div>
        <div>
          <h2>Add new chips:</h2>
          <input
            className={'form-control'}
            type={'text'}
            ref={'category'}
            name="category"
            onChange={() => this.fillCategories()}
          />
          <button onClick={this.addNewCategory}></button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log('markedChips.... ', state);
  return {
  messages: state.data.messages,
  filteredData: state.data.filteredData,
  markedChips: state.data.markedChips || [],
}};
// const mapStateToProps = state => ({
//   messages: state.data.messages,
//   filteredData: state.data.filteredData,
//   markedChips: state.data.markedChips,
// });

const mapDispatchToProps = dispatch => ({
  doSearch: (filtered, all) => dispatch(doSearchCreator(filtered, all)),
  manageMarkedChips: (payload, mark) =>
    dispatch(manageMarkedChipsCreator(payload, mark)),
});

ChipsSelect.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.any),
  filteredData: PropTypes.arrayOf(PropTypes.any),
};

export default connect(mapStateToProps, mapDispatchToProps)(ChipsSelect);
