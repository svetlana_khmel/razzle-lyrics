import React, { Component } from 'react';
import { connect } from 'react-redux';
import Chip from '@material-ui/core/Chip';
import { doSearchCreator } from '../actions';
import PropTypes from 'prop-types';
import '../css/chips.css';

class Chips extends Component {
  constructor(props) {
    super(props);
  }

  searchCategory = e => {
    const query = e.target.dataset.category;
    let queryResult = [];
    const { messages } = this.props;

    messages.map(message => {
      if (
        message &&
        message.data &&
        message.data.category.toLowerCase().indexOf(query.toLowerCase()) != -1
      )
        queryResult.push(message);
    });

    const data = {
      query,
      queryResult,
    };

    this.props.doSearch(data, messages);
  };

  render() {
    let categories = [];
    const { messages } = this.props;

    messages.map(el => {
      if (el.data) {
        if (!categories.includes(el.data.category)) {
          if (/,/.test(el.data.category)) {
            const arr = el.data.category.split(',');

            arr.map(element => {
              if (!categories.includes(element.trim())) {
                categories = [...categories, element.trim()];
              }
            });
          } else {
            categories = [...categories, el.data.category.trim()];
          }
        }
      }
    });

    return (
      <div>
        <div className="filter-block">
          {categories.length !== 0 &&
          categories.map((el, index) => {
            if (el.length !== 0) {
              return (
                <div
                  key={el + index}
                  data-category={el}
                  className="category-item filter"
                  onClick={this.searchCategory}
                >
                  {el}
                </div>
              );
            }
          })}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  messages: state.data.messages,
  filteredData: state.data.filteredData,
});

const mapDispatchToProps = dispatch => ({
  doSearch: (filtered, all) => dispatch(doSearchCreator(filtered, all)),
});

Chips.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.any),
  filteredData: PropTypes.arrayOf(PropTypes.any),
};

export default connect(mapStateToProps, mapDispatchToProps)(Chips);
