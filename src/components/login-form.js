import React from 'react';
import { Field, reduxForm } from 'redux-form';
import renderField from './render-field';
import PropTypes from 'prop-types';

import { required } from '../validation';

const LoginFormContainer = ({ handleSubmit }) => (
  <div className="merchant-form">
    <h2>Login:</h2>
    <form onSubmit={handleSubmit}>
        <Field
          name="username"
          htmlFor="username"
          label="Username"
          component={renderField}
          type="text"
          validate={[required]}
        />
      <Field
          name="password"
          htmlFor="password"
          label="Password"
          component={renderField}
          type="text"
          validate={[required]}
        />
      <button type="submit">
        Submit
      </button>
    </form>
  </div>
);

const LoginForm = reduxForm({
  form: 'loginForm',
})(LoginFormContainer);

LoginFormContainer.propTypes = {
  handleSubmit: PropTypes.func,
};

export default LoginForm;
