import React from 'react';
import PropTypes from 'prop-types';
import Article from './article';

const   LyricsList = ({ filteredData, messages, parentProps, setChoosenPost }) => {

  const commentNodes = (filteredData.length !== 0
    ? filteredData
    : messages
  ).map(message => {
    if (message.data) {
      return (
        <Article
          parentProps={parentProps}
          key={message._id}
          data={message.data}
          id={message._id}
          setChoosenPost={setChoosenPost}
        />
      );
    }
  });

  return <div className="commentList">{commentNodes}</div>;
};

LyricsList.propTypes = {};

export default LyricsList;
