import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';

const styles = theme => ({
  fab: {
    margin: theme.spacing.unit * 2,
  },
  fixed: {
    position: 'fixed',
    bottom: theme.spacing.unit * 1,
    right: theme.spacing.unit * 3,
  },
});

function AddButton(props) {
  const { classes, parentProps } = props;
  console.log('this.props add button', props);

  const clickHandler = () => {
    parentProps.history.push("/new")
  }

  return (
    <div>
      <Tooltip title="Add post">
        <Button onClick={clickHandler} variant="fab" color="secondary" className={classes.fixed}>
          <AddIcon />
        </Button>
      </Tooltip>
    </div>
  );
}

AddButton.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AddButton);