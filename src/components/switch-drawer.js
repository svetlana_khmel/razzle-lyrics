import React, { Component } from 'react';
import TemporaryDrawer from './material-ui/drawer';
import PrimarySearchAppBar from './material-ui/app-bar';

class SwitchDrawer extends Component {
  state = {
    openDriwer: false
  };

  switchDrawerPanel = (open) => {
    this.setState({openDriwer: open})
  }

  render() {
    return (<div>
      <TemporaryDrawer switchDrawerPanel={ this.switchDrawerPanel } openDriwer={this.state.openDriwer} />
      <PrimarySearchAppBar switchDrawerPanel={ this.switchDrawerPanel}  />
    </div>)
  }
}

export default SwitchDrawer;