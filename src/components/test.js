import React from 'react';
import { withRouter } from 'react-router-dom';

class Register extends React.Component {
  handleSubmit = () => {
    this.props.history.push('/edit');
  };
  render() {
    return (
      <div>
        <h1>Register</h1>
        <button onClick={this.handleSubmit} />
      </div>
    );
  }
}

export default withRouter(Register);
