import React from 'react';
import { connect } from 'react-redux';
import { setChoosenPostCreator } from '../actions';
import LyricsList from '../components/lyrics-list';
import PropTypes from 'prop-types';


const LyricsBoxComponent = (props) => {
  const { messages, filteredData, parentProps, setChoosenPost } = props;

    return (
      <div className="commentBox">
        <LyricsList setChoosenPost={setChoosenPost} parentProps={parentProps} filteredData={filteredData} messages={ messages } />
      </div>
    );

}

const mapStateToProps = state => ({
    messages: state.data.messages,
    filteredData: state.data.filteredData
});

const mapDispatchToProps = (dispatch) => ({
  setChoosenPost: (id) => dispatch(setChoosenPostCreator(id))
});

LyricsBoxComponent.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.any),
  filteredData: PropTypes.arrayOf(PropTypes.any)
}

export default connect(mapStateToProps, mapDispatchToProps)(LyricsBoxComponent);


