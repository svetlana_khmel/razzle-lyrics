import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  addArticle,
  loadData,
  setOnlineStatus,
  getStoredDataCreator,
} from '../actions';
import Chips from './Chips';
import RaisedButton from 'material-ui/RaisedButton';
import { Col, Row } from 'react-bootstrap';
import Header from './Header';

class NewPost extends Component {
  constructor(props) {
    super(props);

    this.state = {
      categories: [],
    };
  }

  componentDidMount() {
    const posts = this.props.search.queryResult || this.props.data;

    if (posts.length === 0) {
      this.loadData();
    }
  }

  loadData = () => {
    console.log('Load data..');
    let userdata = JSON.parse(localStorage.getItem('user'));

    this.props.loadData(userdata, getCookie('x-access-token'));
  };

  submitData = () => {
    const title = this.refs.title.value;
    const lyrics = this.refs.lyrics.value;
    const translation = this.refs.translation.value;
    const category = [...this.props.chips, ...this.state.categories];

    const data = {
      title: /\n/.test(title) ? title.split(/\n/) : title || '',
      article: /\n/.test(lyrics) ? lyrics.split(/\n/) : lyrics || '',
      translation: /\n/.test(translation)
        ? translation.split(/\n/)
        : translation || 'Some translation may be here...',
      category: category.length === 0 ? '' : category.join(),
    };

    const token = getCookie('x-access-token');
    let userdata = JSON.parse(localStorage.getItem('user'));

    this.props.addArticle(userdata, token, data);

  };

  fillCategories = () => {
    let categoryValues = this.refs.category.value;

    this.setState({
      categories: [...categoryValues.trim().split(',')],
    });
  };

  choosenChips = () => {
    let length = [...this.props.chips, ...this.state.categories].length;

    let chips = [...this.props.chips, ...this.state.categories].map(
      (el, index) => {
        return length === index + 1 ? <span>{el}</span> : <span>{el}, </span>;
      },
    );

    return chips;
  };

  render() {
    return (
      <div className={'new-post-block'}>
        <Header title={'New Post'} />
        {/*<DisplayForm />*/}

        <form className={'new-post-form'}>
          <fieldset>
            <label>Title</label>
            <input
              className={'form-control'}
              type={'text'}
              ref={'title'}
              name="title"
            />
          </fieldset>
          <fieldset>
            <label>Lyrics</label>
            <textarea className={'form-control'} ref={'lyrics'} name="lyrics" />
          </fieldset>
          <fieldset>
            <label>Translation</label>
            <textarea
              className={'form-control'}
              ref={'translation'}
              name="translation"
            />
          </fieldset>
          <fieldset>
            <label>Category</label>
            <Chips />
          </fieldset>
          <fieldset>
            <div className={'chips '}>{this.choosenChips()}</div>
            <input
              className={'form-control'}
              type={'text'}
              ref={'category'}
              name="category"
              onChange={() => this.fillCategories()}
            />
          </fieldset>
          <fieldset>
            <div className={'floating-bottom-button-block'}>
              <RaisedButton
                className={'floating-bottom-button'}
                label="Send"
                primary={true}
                onClick={this.submitData}
              />
            </div>
          </fieldset>
          <button
            id="something-btn"
            type="button"
            className="btn btn-success btn-sm"
            onClick={this.submitData}
          >
            Send
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return {
    addArticle: bindActionCreators(addArticle, dispatch),
    loadData: bindActionCreators(loadData, dispatch),

    getStoredData: (data, userdata, token) => {
      dispatch(setOnlineStatus(userdata, token));
      dispatch(getStoredDataCreator(data));
    },
    setOnlineStatus: (userdata, token) => {
      dispatch(setOnlineStatus(userdata, token));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewPost);
