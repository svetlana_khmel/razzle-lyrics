import React, {Component} from 'react';
import InputBase from '@material-ui/core/InputBase';
import { doSearchCreator } from '../actions';
import PropTypes from 'prop-types';

import { connect } from 'react-redux'

class SearchInput extends Component {
  constructor(props) {
    super(props);

    this.doSearch = this.doSearch.bind(this);
  }

  doSearch(e) {
    let queryResult = [];
    const query = e.target.value;

    const  { messages }  = this.props;

    messages.map(message => {
      if (message &&
        message.data &&
        message.data.title.toLowerCase().indexOf(query.toLowerCase()) != -1)
        queryResult.push(message);
    });

    const data = {
      query,
      queryResult
    };

    this.props.doSearch(data, messages);
  }

  render() {
    return (
      <div className={'search-input-block'}>
        {/*<input className={'text-input form-control'} type="text" placeholder="Search" ref={'search'} onChange={this.doSearch}/>*/}
        <InputBase
          placeholder="Search…"
          onChange={this.doSearch}
          classes={{
            root: this.props.classes.inputRoot,
            input: this.props.classes.inputInput,
          }}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
    messages: state.data.messages,
    filteredData: state.data.filteredData
  });

const mapDispatchToProps = (dispatch) => ({
    doSearch: (filtered, all) => dispatch(doSearchCreator(filtered, all))
  }
);

SearchInput.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.any),
  filteredData: PropTypes.arrayOf(PropTypes.any)
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchInput)