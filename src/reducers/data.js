import {
  MANAGE_MARKED_CHIPS,
  DO_SEADRCH,
  UPDATE_DATA,
  SET_ACTIVE_POST,
} from '../actions';

const data = (state = {}, action) => {
  switch (action.type) {
    case MANAGE_MARKED_CHIPS:
      const payloadEl = action.payload;
      const currentChips = state.markedChips;

      const markedChips = currentChips.includes(payloadEl)
        ? state.markedChips.filter(chips => chips !== payloadEl)
        : action.mark
          ? [...state.markedChips, payloadEl]
          : state.markedChips.filter(chips => chips !== payloadEl);

      return { ...state, markedChips };

    case DO_SEADRCH:
      const filteredData = { filteredData: action.filtered.queryResult };
      let messages = { messages: action.all };
      return { ...state, ...filteredData, ...messages };

    case SET_ACTIVE_POST:
      const choosenPostId = { choosenPost: action.payload };
      return { ...state, ...choosenPostId };

    case UPDATE_DATA:
      const updatedMessages = { messages: action.payload }
      return { ...state, ...updatedMessages };

    default:
      return state;
  }
};

export default data;
