import { combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';
import data from './data';

const rootReducer = combineReducers({
  data,
  form: reduxFormReducer
});

export default rootReducer;