import App from './App';
import React from 'react';
import { StaticRouter } from 'react-router-dom';
import express from 'express';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import serialize from 'serialize-javascript';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import passport from 'passport';
import localStrategy from 'passport-local';
import expressSession from 'express-session';
import mongoose from 'mongoose';
import config from '../config';
import messages from '../routes/messages';
import Account from '../models/account';
import Messages from '../models/message';
import path from 'path';

const assets = require(process.env.RAZZLE_ASSETS_MANIFEST);
const server = express();

//Auth
server.use(
  expressSession({
    secret: "This is pretty damn weird.",
    resave: false,
    saveUninitialized: false
  })
);

server.use(
  express.static(process.env.NODE_ENV==='production' ? path.join(__dirname, '../build/public') : 'public')
);

// Allows for cross origin domain request:
server.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next()
});

server.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
server.use(bodyParser.json())
server.use(cookieParser());
// server.use(require('express-session')({
//   secret: 'keyboard cat',
//   resave: false,
//   saveUninitialized: false
// }));
server.use(passport.initialize());
server.use(passport.session());

passport.use(new localStrategy(Account.authenticate()));
passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());

server.post('/api/login', function(request, response) {
  passport.authenticate("local", function(err, user, info) {
    if (err) {
      return console.log(err);
    }
    if (!user) {
      console.log("Not authenticated");
      return response.send(false);
    }

    request.logIn(user, function(err) {
      if (err) {
        return console.log(err);
      }
     // checkLogIn(request, response);
      return response.send(checkLogIn(request, response));
    });
  })(request, response);
});


server.post('/api/register', function(req, res, next) {

  Account.register(new Account({username: req.body.username}), req.body.password, function(err) {
    if (err) {
      console.log('error while user register!', err);
      return next(err);
    }

    console.log('user registered!');

    res.send(200);
  });
});

//Auth checkLogIn
function checkLogIn(request, response) {
  if (request.isAuthenticated()) {
     console.log("authenticated");
    return true;
  } else {
    console.log("not authenticated");
    return false;
  }
}

function isLoggedIn(request, response, next) {
  console.log(request);
  if (request.isAuthenticated()) {
    console.log("logged in");
    return next();
  }
  console.log("not logged in");
  return (function() {
    response.send(false);
  })();
}

server.use('/api/messages', messages);

const dbUrl = config[server.settings.env];

console.log('server.settings.env: ', server.settings.env);
console.log("DB URL: ", dbUrl);
console.log("process.env.PORT: ", process.env.PORT);


mongoose.connect(dbUrl).then(
  () => {
    console.log(`MongoDB is connected.`);
  },
  err => {
    console.log(`Mongoose Connection ERROR: ${err}`);
  }
);

server
  .disable('x-powered-by')
  .use(express.static(process.env.RAZZLE_PUBLIC_DIR))
  .get('/*', (req, res) => {

    Messages.find({}, (err, messages) => {
      if (err) {
        console.error('ERROR in GETTING MESSAGES: ',err);
        process.exit(1);
      }
      //var comments = JSON.parse(data);
     // console.error('NO ERROR in GETTING MESSAGES: ',messages);


      const context = {};
      // Compile an initial state

      // const preloadedState = {
      //     filteredData: [],
      //     messages: messages,
      //     counter: 0
      // };

      const preloadedState =  {
        data: {
          filteredData: [],
          messages: messages,
          counter: 0,
          choosenChip: null,
          markedChips: [],
          choosenPost: null
        },
        form: {}
      }

      // Create a new Redux store instance
      const store = configureStore(preloadedState);
      const markup = renderToString(
        <Provider store={store}>
          <StaticRouter context={context} location={req.url}>
            <App />
          </StaticRouter>
        </Provider>
      );

      // Grab the initial state from our Redux store
      const finalState = store.getState();

      if (context.url) {
        res.redirect(context.url);
      } else {
        res.status(200).send(
          `<!doctype html>
    <html lang="">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta charset="utf-8" />
        <title>Welcome to Razzle</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no" />
        ${
            assets.client.css
              ? `<link rel="stylesheet" href="${assets.client.css}">`
              : ''
            }
        ${
            process.env.NODE_ENV === 'production'
              ? `<script src="${assets.client.js}" defer></script>`
              : `<script src="${assets.client.js}" defer crossorigin></script>`
            }
    </head>
    <body>
        <div id="root">${markup}</div>
        <script>
          window.__PRELOADED_STATE__ = ${serialize(finalState)}
        </script>
    </body>
</html>`
        );
      }
    });
  });

export default server;
